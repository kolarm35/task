import scrapy
from selenium import webdriver
from scrapy.selector import Selector
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import chromedriver_autoinstaller

chromedriver_autoinstaller.install()
                                    
class FlatsspiderSpider(scrapy.Spider):
    name = "flatsspider"
    allowed_domains = ["www.sreality.cz"]
    page = 1
    start_url = "https://www.sreality.cz/hledani/prodej/byty?strana={page}"
    title_css = 'div > div > div > span > h2 > a > span'
    img_url_css = 'div > preact > div > div > a:nth-child(1) > img'
    div_css = 'div.dir-property-list'

    def start_requests(self):
        yield scrapy.Request(url=self.start_url.format(page=self.page), callback=self.parse)

    def parse(self, response):
        chrome_options = ChromeOptions()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--sandbox")
        chrome_options.add_argument('--disable-dev-shm-usage')
        
        driver = webdriver.Chrome(options=chrome_options)
        
        try:
            driver.get(response.url)
            
            WebDriverWait(driver, 1).until(EC.presence_of_element_located((By.CSS_SELECTOR, self.div_css)))

            sel = Selector(text=driver.page_source)

            for selector in sel.css(self.div_css):
                yield {
                    "page": self.page,
                    "title": selector.css(self.title_css + '::text').getall(),
                    "img_url": selector.css(self.img_url_css + '::attr("src")').getall()
                }
        finally:
            driver.quit()

        if self.page < 25:
            self.page += 1
            yield scrapy.Request(url=self.start_url.format(page=self.page), callback=self.parse)
