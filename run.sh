#!/bin/bash

# Install Python requirements
pip install -r requirements.txt

# Run Scrapy spider
cd scrapyproject
scrapy crawl -o res.json flatsspider
cd ..

# Start PostgreSQL session and execute SQL commands non-interactively
sudo -iu postgres psql -c "CREATE DATABASE task_db;"
sudo -iu postgres psql -c "CREATE USER task_user WITH PASSWORD '12345';"
sudo -iu postgres psql -c "ALTER ROLE task_user SET client_encoding TO 'utf8';"
sudo -iu postgres psql -c "ALTER ROLE task_user SET default_transaction_isolation TO 'read committed';"
sudo -iu postgres psql -c "ALTER ROLE task_user SET timezone TO 'UTC';"
sudo -iu postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE task_db TO task_user;"

# Run Python scripts
python3.11 database.py
python3.11 web.py

