from flask import Flask, render_template
import psycopg2

app = Flask(__name__)

@app.route('/')
def index():
    conn = psycopg2.connect(
        database="task_db",
        user="task_user",
        password="12345",
        host="localhost", 
        port="5432"
    )

    cur = conn.cursor()

    cur.execute("SELECT title, img_url FROM sreality_ads LIMIT 500")
    data = cur.fetchall()

    cur.close()
    conn.close()

    return render_template('index.html', data=data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
