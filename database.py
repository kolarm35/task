import psycopg2

conn = psycopg2.connect(
    dbname="task_db",
    user="task_user",
    password="12345",
    host="localhost",
    port="5432"
)

cur = conn.cursor()
create_table_sql = """
CREATE TABLE IF NOT EXISTS sreality_ads (
    id SERIAL PRIMARY KEY,
    title TEXT,
    img_url TEXT
);
"""
cur.execute(create_table_sql)

import json

with open('scrapyproject/res.json', 'r') as json_file:
    data = json.load(json_file)

for item in data:
    title = item.get('title', '')
    img_url = item.get('img_url', '')
    for i in range(len(title)):
        cur.execute("INSERT INTO sreality_ads (title, img_url) VALUES (%s, %s)", (title[i], img_url[i]))

conn.commit()

cur.close()
conn.close()
